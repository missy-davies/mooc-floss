terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.37.0"
    }
  }
  required_version = ">= 0.14.0"
}

variable "key_pair_name" {}

# get an image named "Ubuntu 20.04"
data "openstack_images_image_v2" "ubuntu_20_04" {
  name        = "Ubuntu 20.04"
  most_recent = true
}

# get a flavor named "s1-8", 2 vCPUS and 8gb RAM
data "openstack_compute_flavor_v2" "s1-8" {
  name = "s1-8"
}

resource "openstack_compute_secgroup_v2" "gitlab" {
  description = "Gitlab server security group"
  name        = "gitlab"

  rule {
    from_port   = 22
    ip_protocol = "tcp"
    to_port     = 22
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 80
    ip_protocol = "tcp"
    to_port     = 80
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 443
    ip_protocol = "tcp"
    to_port     = 443
    cidr        = "0.0.0.0/0"
  }
}

resource "openstack_compute_instance_v2" "basic" {
  name            = "gitlab"
  image_id        = data.openstack_images_image_v2.ubuntu_20_04.id
  flavor_id       = data.openstack_compute_flavor_v2.s1-8.id
  key_pair        = var.key_pair_name
  security_groups = [openstack_compute_secgroup_v2.gitlab.id]

  network {
    name = "Ext-Net"
  }
}

output "instance_ip_addr" {
  value = openstack_compute_instance_v2.basic.network.0.fixed_ip_v4
}