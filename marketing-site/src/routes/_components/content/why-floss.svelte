<script lang="typescript">
  import {GridWrap, GridWrapModifier, GridColumn, GridColumnModifier} from '$lib/components/grid';
  import {Wrap} from '$lib/components/wrap';
  import {Circle} from '$lib/components/circle';
  import {Icon, IconId} from '$lib/components/icon';
  import {Island, IslandModifier} from '$lib/components/island';
  import {CaptionedImage} from '$lib/components/image';
  import {AnimatedSvgText} from '$lib/components/animated-svg';

  import {urls} from '$lib/data/urls';
  import {resolveStaticAssetPath} from '$lib/utils/components';

  import type {FragmentId} from '$lib/enums/fragment-ids';

  import whyFlossUnderlineSvg from '$assets/img/svg/doodles/doodle-why-floss.svg?src';
  import freedomsSvg from '$assets/img/svg/doodles/doodle-freedoms.svg?src';
  import usersRightsSvg from '$assets/img/svg/doodles/doodle-user-rights.svg?src';
  import futureUnderlineSvg from '$assets/img/svg/doodles/doodle-future.svg?src';

  import helloFutureUrl from '$assets/img/svg/hello-future.svg?url';

  export let fragmentId: FragmentId;

  const freedoms = [
    'The freedom to run software as you wish, for any purpose',
    'The freedom to study how the software works, and adapt it to your needs',
    'The freedom to redistribute copies of the software to help others',
    'The freedom to distribute copies of your modified versions to others, for the benefit of the community',
  ];
  const altSuffix = 'CC image courtesy of Publicity Pod on Flickr';
  const caption = `CC image courtesy of
    <a href="${urls.publicityPod}">Publicity Pod</a> on Flickr
  `;
</script>

<section id={fragmentId}>
  <div class="u-bgc--grey-dk u-margin--largest--inline u-margin--inline--palm">
    <div class="absolute-context">
      <Island modifiers={[IslandModifier.Smallest]}>
        <Wrap>
          <GridWrap modifiers={[GridWrapModifier.Larger]}>
            <GridColumn baseWidth="2-of-3" viewportMap={{portable: '1-of-1'}}>
              <h2 class="u-fs--h5">
                Why
                <AnimatedSvgText duration=".5s" svg={whyFlossUnderlineSvg}>FLOSS?</AnimatedSvgText>
              </h2>

              <p>
                FLOSS has proven to be an efficient way to create quality, reliable software. FLOSS
                is dedicated to the principles of freedom, collaboration, and community, and we
                believe it has the power, not only to improve society, but to change the world.
              </p>
            </GridColumn>

            <GridColumn baseWidth="1-of-3" viewportMap={{portable: '1-of-1'}}>
              <CaptionedImage
                class="circle-content"
                src={resolveStaticAssetPath(
                  'img/stock/student-sitting-in-a-coffee-shop-with-their-laptop.jpg',
                )}
                alt="Student sitting in a coffee shop with their laptop. {altSuffix}"
                >{@html caption}</CaptionedImage
              >
            </GridColumn>
          </GridWrap>

          <Island modifiers={[IslandModifier.SmallerStart]}>
            <GridWrap>
              <GridColumn
                class="g--push-desk-1-of-4"
                baseWidth="2-of-3"
                viewportMap={{portable: '1-of-1'}}
              >
                <h3 class="u-fs--h5">
                  FLOSS is built on
                  <AnimatedSvgText svg={freedomsSvg}>&#8196;4&#8196;</AnimatedSvgText>
                  essential freedoms:
                </h3>

                {#each freedoms as freedom}
                  <GridWrap modifiers={[GridWrapModifier.Small]}>
                    <GridColumn modifiers={[GridColumnModifier.ShrinkWrap]}>
                      <Circle>
                        <Icon id={IconId.Tick} />
                      </Circle>
                    </GridColumn>
                    <GridColumn modifiers={[GridColumnModifier.Auto]}>
                      <p>
                        {freedom}
                      </p>
                    </GridColumn>
                  </GridWrap>
                {/each}

                <GridWrap modifiers={[GridWrapModifier.Small]}>
                  <GridColumn modifiers={[GridColumnModifier.ShrinkWrap]}>
                    <div style="visibility:hidden">
                      <Circle />
                    </div>
                  </GridColumn>
                  <GridColumn modifiers={[GridColumnModifier.Auto]}>
                    <p>
                      So when you hear the “free” in FLOSS, think of “freedom” and “liberty”; don’t
                      think of “free coffee”!
                      <a href={urls.flossDefinition}>(Source)</a>
                    </p>
                  </GridColumn>
                </GridWrap>
              </GridColumn>
            </GridWrap>
          </Island>

          <Island modifiers={[IslandModifier.SmallerStart]}>
            <GridWrap>
              <GridColumn baseWidth="3-of-5" viewportMap={{portable: '1-of-1'}}>
                <h3 class="u-fs--h5">
                  FLOSS respects the user’s
                  <AnimatedSvgText duration=".3s" svg={usersRightsSvg}>rights</AnimatedSvgText>
                </h3>

                <p>
                  The Four Freedoms outlined above are the cornerstone of the FLOSS movement. They
                  protect the software user’s human rights, and ensure that their civil liberties
                  are respected.
                </p>

                <p>
                  FLOSS projects put the control in the user’s hands by allowing them to view,
                  modify, and distribute software without having to depend on a third-party.
                </p>
              </GridColumn>
            </GridWrap>
          </Island>

          <Island modifiers={[IslandModifier.SmallerStart]}>
            <GridWrap modifiers={[GridWrapModifier.Larger]}>
              <GridColumn baseWidth="1-of-3" viewportMap={{portable: '1-of-1'}}>
                <p class="u-margin--negative--large--block-start--desk">
                  <CaptionedImage
                    class="circle-content"
                    src={resolveStaticAssetPath(
                      'img/stock/two-developers-working-together-to-solve-a-problem.jpg',
                    )}
                    alt="Two developers working together to solve a problem. {altSuffix}"
                    >{@html caption}</CaptionedImage
                  >
                </p>
              </GridColumn>

              <GridColumn baseWidth="2-of-3" viewportMap={{portable: '1-of-1'}}>
                <h3 class="u-fs--h5">
                  FLOSS is the way of the
                  <AnimatedSvgText svg={futureUnderlineSvg}>future</AnimatedSvgText>
                </h3>

                <p>
                  We believe that FLOSS is a better way to create software by allowing varied actors
                  to pull their efforts together and collaborate on projects. Well-managed, the end
                  result is better than anyone could achieve on their own, with less duplication.
                  Collaborating on FLOSS projects requires a specific approach and set of skills,
                  which is largely misunderstood and rare today. That’s why we’re designing this
                  course - we believe FLOSS is the way of the future, and we can't wait to see more
                  people join the movement and experience it for themselves.
                </p>
              </GridColumn>
            </GridWrap>
          </Island>
        </Wrap>
      </Island>

      <div class="absolute-context__content absolute-context__content--bottom-right">
        <img
          class="u-translate-3d u-translate-3d--portable"
          style={`
        --translate-x: -25%;
        --translate-y: 60%;
        --translate-x-portable: -10%;
        --translate-y-portable: 50%;
        `}
          src={helloFutureUrl}
          alt="Illustration of a smiley face with text 'Hello future'"
        />
      </div>
    </div>
  </div>
</section>
