interface ActionReturn {
  update?: (newOptions: IntersectionObserverInit) => void;
  destroy: () => void;
}

export enum IntersectionEvent {
  Entry = 'intersectionentry',
  Exit = 'intersectionexit',
}

/**
 * intersection
 *
 * Svelte action
 *
 * Dispatch events when a node enters or exits the viewport.
 *
 * Usage:
 *  <div use:intersection on:intersectionentry={doSomething} />
 *
 * @param {HTMLElement} node
 * @param {string} childSelector
 */
function intersection(
  node: HTMLElement,
  options: IntersectionObserverInit = {threshold: 1},
): ActionReturn {
  let observer: IntersectionObserver;

  function handleEntries(entries: IntersectionObserverEntry[]) {
    entries.map((entry) => {
      const eventName = entry.isIntersecting ? IntersectionEvent.Entry : IntersectionEvent.Exit;

      node.dispatchEvent(new CustomEvent(eventName, {detail: entry}));
    });
  }

  if (typeof IntersectionObserver !== 'undefined') {
    observer = new IntersectionObserver(handleEntries, options);

    observer.observe(node);
  }

  return {
    destroy() {
      if (observer) {
        observer.disconnect();
      }
    },
  };
}

export {intersection};
